
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))

    it("Fills all the text input fields", () => {
        const firstName = "John";
        const lastName = "Doe";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("talkingabouttesting@gmail.com");
        cy.get("#requests").type("vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    })

    it("Select 'vip' ticket type", () => {
        cy.get("#vip").check();
    })

    it("Selects 'social media' checkbox", () => {
        cy.get("#social-media").check()
    })

    it("Selects 'friend' and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#friend").uncheck()
    })

    it("Has  'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain","TICKETBOX")
    })

    it("Alerts on invelid email", () => {
        cy.get("#email")
            .as("email")
            .type("talkingabouttesting-gmail.com")

        cy.get("#email.invalid")
            .as("invalidEmail")
            .should("exist")

        /* Alias use - Alias saves the initial status so should not be used for 
            sequential evaluation*/
        cy.get("@email")
            .clear()
            .type("talkingabouttesting@gmail.com")

        cy.get("#invalid-email").should("not.exist")
    })

    it("Fills and reset the form",() => {
        const firstName = "John";
        const lastName = "Doe";
        const fullname = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("talkingabouttesting@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullname}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click()
        cy.get("#signature").type(fullname)

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled")

        cy.get("button[type='reset']").click()

        cy.get("@submitButton").should("be.disabled")
    })

    it("Fills mandatory fields using mandatory comand", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@example.com"
        }

        cy.fillMandatoryFields(customer)

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled")

        cy.get("#agree").uncheck()

        cy.get("@submitButton").should("be.disabled")

    })
})